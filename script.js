// ** Script description
// This script is split up into different "parts" which each represent
// different areas of the app.
//
// * DOM Nodes - The dom nodes that are in use
// * Application Data - The "state" of the app
// * Event listeners - Event listeners that are hooked up
// * Object creators - Blueprints for objects used in the app
// * Mocks - Mock data is added here upon Initializing the app
// * Functions - Main functions of the app
// * Helper Functions - Smaller functions that are used in the main functions
// * Initialization - Initializes the app




//######################## DOM Nodes ##################################//

// Input/Search field
const inputBox = document.getElementById('input-container');
const inputField = document.getElementById('input-term');
const addButton = document.getElementById('add-button');

//Tag box
const tagBox = document.getElementById('tag-container');
const allTags = document.querySelectorAll('div.tag');

//List items container
const itemsBox = document.getElementById('item-container');

//########################################################################//


//######################## Application Data ##################################//

const applicationData = {
  todos: [],
  tags: [],
  filter: ''
}

//########################################################################//


//######################## Event listeners ##################################//

// Event listener for clicking the input container, focuses input field
inputBox.addEventListener('click', () => {
  inputField.focus();
})

inputBox.addEventListener('click', () => {
  clearFilter();
  updateView();
})

// Event listener for clicking the 'add' button
addButton.addEventListener('click', () => {
  addItemFromInput();
})

// Event listener for "Enter" key press while input field is focused
inputBox.addEventListener('keypress', (event => {
  // Checks if pressed key is Enter key
  if(event.keyCode == 13){
    addItemFromInput();
  }
}));

// Event listener for all tags setting a filter and rerendering on click.
document.addEventListener('click', event => {
  if(event.target.className == 'tag'){
    // Update filter in applicationData
    setFilter(event.target.innerText);
    // Update view, passing filter type
    updateView('tags');
  }
});

// Event listener for changes in the input field, setting a filter and rerendering on click.
inputField.addEventListener('input', (event => {
    // Set the filter term
    setFilter(event.target.value);
    // Update view, passing filter type
    updateView('title');
  ;
}))

// Event listener for checking a checkbox
document.addEventListener('click', event => {
  if(event.target.type == 'checkbox'){
    // Call the function "checkElement" passing the name (id) of the input
    checkElement(event.target.name);
  }
})



//########################################################################//


//######################## Object creators ##################################//

// Todo object blueprint
function TodoObject(object) {
  this.id = object.id;
  this.title = object.title;
  this.tags = [...object.tags];
  this.completed = false;

  // Method for completing/uncompleting task
  this.changeCompleted = () => {
    this.completed = !this.completed;
  }
}

// Tag object blueprint
function TagObject(object) {
  this.title = object.title.toLowerCase();
  this.color = object.color;
}

//########################################################################//


//######################## Mocks ##################################//


function createMocks() {
  let mockTag1 = new TagObject({
    title: 'bug',
    color: '#AAff00'
  });

  let mockTag2 = new TagObject({
    title: 'frontend',
    color: '#1fbfbf'
  });

  let mockItem1 = new TodoObject({
    title: 'This is a todo item',
    id: '0000',
    tags: [mockTag1]
  })

  let mockItem2 = new TodoObject({
    title: 'This is another todo item',
    id: '1111',
    tags: [mockTag1, mockTag2]
  })


  applicationData.tags.push(mockTag1, mockTag2)
  applicationData.todos.push(mockItem1, mockItem2);
}


//########################################################################//


//######################## Functions ##################################//

// Initializes application
function initApp() {
  createMocks();
  updateView();
}

// Updates view with data from applicationData
function updateView(filter = '') {
  updateTags();
  // Pass filter to updateItems
  updateItems(filter);
}

// Adds item from input field
function addItemFromInput() {
  // Grab value from inputField
  const todoItem = inputField.value;
  // Initialize variables
  let inputTags, inputText, todo, htmlElement;
  let todoTags = [];

  // Check that there is a value in the field. If not, return
  if(isEmpty(todoItem)) return;
  // Grab tags
  inputTags = grabTags(todoItem);
  // Grab text without tags
  inputText = grabText(todoItem);
  // Get tag objects from input tags
  todoTags = [...getTagObjects(inputTags)];
  //Create list-item object with the tag objects
  todo = new TodoObject({
    id: randomID(),
    title: inputText,
    tags: todoTags
  });
  //Add the new todo to the application data
  applicationData.todos.push(todo);
  //Clear input field
  clearInput();
  //Update view to add the new todo item and tags
  updateView();
}

function checkElement(id) {
  let matchingObject = [];
  let matchingElement;

  // Filter out the todo object with the matching id
  matchingObject = applicationData.todos.filter(todo => {
    return (todo.id == id);
  });

  // Call the checked method of the object
  matchingObject[0].changeCompleted();

  // Add or remove the checked class to the objects HTML element
  // Find the matching element using the id
  matchingElement = document.getElementById(id);
  // If it is checked, add the checked class, else, remove it
  if(matchingObject[0].completed){
    matchingElement.classList.add('checked');
  } else {
    matchingElement.classList.remove('checked');
  }
}

//########################################################################//


//######################## Helper Functions ##################################//

// Sets filter term
function setFilter(string) {
  // Convert string to upper case to ignore casing
  const filter = string.toUpperCase()
  applicationData.filter = filter;
}

//Clear filter term
function clearFilter(type) {
  applicationData.filter = '';
}

// Filters todos by tags
function filterByTag(todos) {
  return todos.filter(todo => {
    const tags = [];
    const filterTerm = applicationData.filter;

    todo.tags.forEach(tag => tags.push(tag.title.toUpperCase()));

    if(tags.includes(filterTerm)) return true;
  })
};


// Filtes todos by todo title
function filterByTitle(todos) {
  return todos.filter(todo => {
    const filterTerm = applicationData.filter;

    if(todo.title.toUpperCase().includes(filterTerm)) return true;
  })
}

// Filters todos according to type and filter term.
function filterTodos(todos, filter) {
  let filteredTodos = [];
  const filterTerm = applicationData.filter;

  // If no filter term, return todos as is
  if(filterTerm == '') return todos;

  // Call filter depending on type
  switch(filter) {
    case 'tags':
      filteredTodos = filterByTag(todos);
      break;
    case 'title':
      filteredTodos = filterByTitle(todos);
      break;
    default:
      filteredTodos = todos;
  }

  //Return filtered list of todos
  return filteredTodos;
};


// Resets input field
function clearInput() {
  inputField.value = '';
};

// Checks if the passed string is empty
function isEmpty(string) {
  return string === '';
}

// Returns all tags found in the passed string
function grabTags(string) {
  let tags = [];
  let tag;

  // Rules to match text following a hashtag until a space
  const regexRules = /(?:^|\s)(?:#)([a-zA-Zå-öÅ-ÖäÄ\d]+)/gm;

  // Push the text followed by '#' for all matches found
  while ((tag = regexRules.exec(string))) {
    tags.push(tag[1])
  }

  return tags;
}

// Returns text cleaned from tags
function grabText(string) {
  // Rules to match text following a hashtag until a space
  const regexRules = /(?:^|\s)(?:#)([a-zA-Zå-öÅ-Öä-öÄ-Ö\d]+)/gm;
  // Initialize variables
  let tags = [];
  let tag;
  let cleanedString = string;

  // Push the hashtags to the tags list
  while((tag = regexRules.exec(string))) {
    tags.push(tag[0])
  }

  // Replace the tags in the string by ''
  for(let i = 0; i < tags.length; i++){
    cleanedString = cleanedString.replace(tags[i], '');
  }

  return cleanedString;
};

// Returns an array of tag objects from an array of tag strings
function getTagObjects(tags) {
  const tagObjects = [];
  const newTags = [];
  let newTagObjects;

  // Check if tags matches any tags in store. If so, add them to todoTags. matchOldTags returns a list of matching tags.
  tagObjects.push(...matchOldTags(tags));
  // Add new tags to the newTags array. filterNewTags returns a list of new tags.
  newTags.push(...filterNewTags(tags))
  // Create new tag objects from the new tags. This also pushes them to applicationData.
  newTagObjects = createTagObjects(newTags);
  // Push new tag objects to tagObjects
  tagObjects.push(...newTagObjects);

  return tagObjects;
}

// Creates new tag objects from a list of tag titles
function createTagObjects(tags) {
  const newTagObjects = [];

  tags.forEach(tag => {
    //For each tag, create a new object
    let newTagObject = new TagObject({
      title: tag,
      color: randomColor()
    })

    //Push the new object to the list of new tag objects
    newTagObjects.push(newTagObject);

    //Push the new object to applicationData
    applicationData.tags.push(newTagObject);
  })

  //Return all the new objects
  return newTagObjects;
};

// Checks if tags are already in store, and if so, returns them
function matchOldTags(tags) {
  let matches = [];
  // for each tag
  tags.forEach(tag => {
    //Go through the tags in store
    applicationData.tags.forEach(storeTag => {
      //if the tag is a match
      if (storeTag.title === tag) {
        //Add the tag from store to matches
        matches.push(storeTag);
      }
    })
  })

  return matches;
};

// Checks which tags are new and returns them in a list
function filterNewTags(tags) {
  let newTags = [];
  let oldTags = [];

  // Get all old tag titles from applicationData
  applicationData.tags.forEach(tag => {
    oldTags.push(tag.title)
  });

  // Check which tags are new
  tags.forEach(tag => {
    if(!oldTags.includes(tag)){
      newTags.push(tag);
    }
  })

  return newTags;
}

// Creates a HTML element for each passed tag with the tags color and title
function createTagHtmlElements(tags) {
  const htmlTags = [];

  tags.forEach(tag => {
    htmlTags.push(`<div class="tag" style="background-color: ${tag.color}">${tag.title}</div>`);
  });

  return htmlTags;
}

// Creates a HTML element for the passed todoObject with tags
function createTodoHtmlElement(todo, tags) {
  return (`
    <div class="item ${todo.completed ? 'checked' : null}" id="${todo.id}">
      <div class="item-text">
        <input type="checkbox" ${todo.completed ? 'checked' : null} name="${todo.id}">
        <label for="${todo.id}">${todo.title}</label>
      </div>
      <div class="item-tags">
        <div class="item-tags">
          ${tags.join(' ')}
        </div>
      </div>
    </div>
  `);
}

//Updates the tags on the page with data from applicationData
function updateTags() {
  let tags = [];

  // Sort the tags alphabetically
  sortTags(applicationData.tags)

  //Creates a HTML element for each tag with the tags color and title
  tags = [...createTagHtmlElements(applicationData.tags)]

  //Joins all tags together to an HTML string
  const tagElements = tags.join(' ')

  // Updates the view
  tagBox.innerHTML = tagElements;
}

function updateItems(filterType = '') {
  const items = [];
  let filteredTodos = [];

  //Filter out the todos not matching the filter term if there is one
  filteredTodos = filterTodos(applicationData.todos, filterType)

  // Clear filter after it's been used
  clearFilter();

  //Creates a HTML element for each item
  filteredTodos.forEach(todo => {
    let tagElements = [];
    let todoElement;

    tagElements = [...createTagHtmlElements(todo.tags)]
    todoElement = createTodoHtmlElement(todo, tagElements)
    items.push(todoElement);
  })

  // Update window
  itemsBox.innerHTML = items.join('')
};


// Generates a random color
function randomColor() {
  //Create a list of all possible values
  let values = '0123456789ABCDEF'.split('');
  let color = '#';
  //Add 6 random values
  for (let i = 0; i < 6; i++) {
    color += values[Math.floor(Math.random() * 16)];
  }

  return color;
};

// Generates a random ID
function randomID() {
  return Math.random().toString(36).substr(2, 10);
};

// Sorts a list of tag objects alphabetically
function sortTags(tagObjects) {
  // Sorts the tags alphabetically, converting them to upper case to ignore casing.
  tagObjects.sort((a,b) => {
    let titleA = a.title.toUpperCase();
    let titleB = b.title.toUpperCase();
    return (titleA < titleB) ? -1 : (titleA > titleB) ? 1 : 0;
  });
};


//########################################################################//


//######################## Initialization ##################################//

initApp();
